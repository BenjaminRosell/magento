<?php 
class Megnor_Bestseller_Block_Bestseller extends Mage_Core_Block_Template { 

	const DEFAULT_PRODUCTS_COUNT = 5;	
	
	public function _prepareLayout()
    {
		
    }
	
	public function getBestProductCollection()
    {    	
        $storeId    = Mage::app()->getStore()->getId();       
		
		$visibility = array(
                      Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                      Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
                  );
	
		$products = Mage::getResourceModel('reports/product_collection')
                              ->addAttributeToSelect('*')
                              ->addOrderedQty()
							  ->setPageSize($this->getProductsCount())
                              ->addAttributeToFilter('visibility', $visibility)
                              ->setOrder('ordered_qty', 'desc');

		return $products; 				
    }		
	
	protected function _toHtml() {
        if (!$this->helper('bestseller')->getIsActive()) {
            return '';
        }
        return parent::_toHtml();
    }

	
	public function setProductsCount($count) {
        $this->_productsCount = $count;
        return $this;
    }

    public function getProductsCount() {		
		$count = Mage::getStoreConfig('bestseller/sidebar/number_of_items');
				
		if($count) 		
			return $count;
			
        if (null === $this->_productsCount) {
            $this->_productsCount = self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->_productsCount;
    }
	
	public function getSidebarHeading() {		
		return Mage::getStoreConfig('bestseller/sidebar/heading');		
    }

}  