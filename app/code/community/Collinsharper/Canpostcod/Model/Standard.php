<?php

class Collinsharper_Canpostcod_Model_Standard extends Mage_Payment_Model_Method_Abstract
{


    protected $_code = 'chcanpostcod';

    protected $_formBlockType = 'chcanpostcod/standard_form';
    protected $_infoBlockType = 'chcanpostcod/standard_info';


    protected $_isGateway               = false;
    protected $_canAuthorize            = false;
    protected $_canCapture              = false;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;


    public function isAdmin()
    {
        return (Mage::app()->getStore()->isAdmin() || Mage::getDesign()->getArea() == 'adminhtml');
    }

    public function isCanadaPostCod()
    {
        $params = array();
        if($this->isAdmin()) {
            $session = Mage::getSingleton('adminhtml/session_quote');
            if($session) {
                $quoteId = $session->getQuoteId();
            }
        } else {
            $session = Mage::getSingleton('checkout/session');
            $quoteId = $session->getQuote()->getId();
        }


        if($quoteId) {
            $params = Mage::getModel('chcanpost2module/quote_param')->getParamsByQuote($quoteId);
        }

        return isset($params['cod']) && $params['cod'] == 1;
    }

    public function canUseCheckout()
    {
        return ($this->getConfigData('activefe') == true && $this->isCanadapostCod());
    }


    public function canUseInternal()
    {
        return $this->isCanadapostCod();
    }


    public function canUseForMultishipping()
    {
        return false;
    }


    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('chcanpostcod/standard_form', $name)
            ->setMethod('chcanpostcod')
            ->setPayment($this->getPayment())
            ->setTemplate('canpostcod/standard/form.phtml');

        return $block;
    }

    public function createInfoBlock($name)
    {
        $block = $this->getLayout()->createBlock('chcanpostcod/standard_info', $name)
            ->setPayment($this->getPayment())
            ->setTemplate('canpostcod/standard/info.phtml');

        return $block;
    }

    public function assignData($data)
    {
        $details = array();

        if ($this->getWiretransferInstructions()) {
            $details['chcanpostcod_instructions'] = $this->getWiretransferInstructions();
        }
        if ($this->getWiretransferInstructionsEmail()) {
            $details['chcanpostcod_instructionsEmail'] = $this->getWiretransferInstructionsEmail();
        }
        if (!empty($details)) {
            $this->getInfoInstance()->setAdditionalData(serialize($details));
        }
        return $this;
    }

    public function getWiretransferInstructions()
    {
        return $this->getConfigData('wire_instructions');
    }


    public function getWiretransferInstructionsEmail()
    {
        return $this->getConfigData('wire_instructions_email');
    }

}
