<?php


class Collinsharper_Canpostcod_Block_Standard_Info extends Mage_Payment_Block_Info
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('canpostcod/standard/info.phtml');
    }
}
