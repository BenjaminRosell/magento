<?php

class Collinsharper_Canpostcod_Block_Standard_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        $this->setTemplate('canpostcod/standard/form.phtml');
        parent::_construct();
    }
}
