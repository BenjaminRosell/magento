<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$attribute = new Mage_Eav_Model_Entity_Attribute();

$attribute->loadByCode('catalog_product', 'hs_tariff_code');

if($attribute->getAttributeId() == '') {

    $installer->addAttribute('catalog_product', 'hs_tariff_code', array(
            'group' => 'General',
            'type' => 'varchar',
            'backend' => '',
            'frontend' => '',
            'label' => 'Hs Tariff Code',
            'note' => 'Hs Tariff Code',
            'input' => 'text',
            'class' => '',
            'source' => 'eav/entity_attribute_source_table',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'default' => '0',
            'visible_on_front' => false,
            'unique' => false,
            'is_configurable' => false,
            'used_for_promo_rules' => true
        ));

}

$installer->endSetup();