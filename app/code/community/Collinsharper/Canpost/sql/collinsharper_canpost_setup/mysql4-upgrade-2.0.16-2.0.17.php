<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$provs = $this->_conn->fetchAll('SELECT
                                    region_id,
                                    code,
                                    default_name
                                 FROM
                                    '.$this->getTable('directory/country_region').'
                                 WHERE country_id=?', 'CA');

foreach ($provs  as $prov) {

    $options[$prov['code']] = $prov['default_name'];

}

$installer->addAttribute('catalog_product','origin_province', array(
                                            'group'             => 'General',
                                            'type'              => 'varchar',
                                            'input'             => 'select',
                                            'label'             => 'Province of Origin',
                                            'backend'           => 'eav/entity_attribute_backend_array',
                                            'frontend'          => '',
                                            'source'            => '',
                                            'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                                            'visible'           => true,
                                            'required'          => false,
                                            'user_defined'      => false,
                                            'searchable'        => true,
                                            'filterable'        => true,
                                            'comparable'        => true,
                                            'option'            => array (
                                                'values' => $options
                                            ),
                                            'default' => '0',
                                            'visible_on_front'  => true,
                                            'visible_in_advanced_search' => true,
                                            'unique'            => false
                                        ));

$entityTypeId = $installer->getEntityTypeId('catalog_product');
$sets = $this->_conn->fetchAll('select * from '.$this->getTable('eav/attribute_set').' where entity_type_id=?', $entityTypeId);

foreach ($sets as $set) {

    $installer->addAttributeToSet('catalog_product', $set['attribute_set_id'], 'General', 'origin_province', 101);

}

$installer->endSetup();