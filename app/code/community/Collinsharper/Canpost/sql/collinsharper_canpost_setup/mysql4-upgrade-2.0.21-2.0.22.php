<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$restrictShippingMethods = 'restrict_shipping_methods';
$attrId = $installer->getAttributeId('catalog_product', $restrictShippingMethods);
if (!$attrId) {
    $installer->addAttribute('catalog_product', $restrictShippingMethods, array(
        'group'             => 'General',
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Restrict Shipping Methods',
        'note'              => 'Restrict shipping methods to those selected below (Allowed Shipping Methods)',
        'input'             => 'select',
        'sort'              => '5000',
        'class'             => '',
        'source'            => 'eav/entity_attribute_source_boolean',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'default'           => '0',
        'visible_on_front'  => false,
        'unique'            => false,
        'is_configurable'   => false,
        'used_for_promo_rules'  => false
    ));
}

$allowedMethodsCode = 'allowed_shipping_methods';
$attrId = $installer->getAttributeId('catalog_product', $allowedMethodsCode);
if (!$attrId) {
    $installer->addAttribute('catalog_product', $allowedMethodsCode, array(
        'group'             => 'General',
        'type'              => 'varchar',
        'backend'           => 'eav/entity_attribute_backend_array',
        'frontend'          => '',
        'label'             => 'Allowed Shipping Methods',
        'note'              => 'Only the selected shipping methods will be allowed via Canada Post if Restrict Shipping Methods is enabled.',
        'input'             => 'multiselect',
        'sort'              => '5001',
        'class'             => '',
        'source'            => 'chcanpost2module/catalog_product_attribute_source_method',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'default'           => '',
        'visible_on_front'  => false,
        'unique'            => false,
        'is_configurable'   => false,
        'used_for_promo_rules'  => false
    ));
}

$installer->endSetup();
