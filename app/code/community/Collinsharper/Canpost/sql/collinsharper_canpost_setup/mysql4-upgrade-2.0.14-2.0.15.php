<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

try {
    $installer->run("ALTER TABLE `ch_canadapost_quote_param` ADD COLUMN est_delivery_date INT(11) NOT NULL");
} catch (Exception $e) {
    // Check if we're just trying to re-run scripts and the column already exists.
    if (strpos($e->getMessage(), "SQLSTATE[42S21]: Column already exists") === false) {
        throw $e;
    }
}

try {
    $installer->run("ALTER TABLE `ch_canadapost_quote_param` ADD COLUMN coverage_amount DECIMAL(12,4) NOT NULL");
} catch (Exception $e) {
    // Check if we're just trying to re-run scripts and the column already exists.
    if (strpos($e->getMessage(), "SQLSTATE[42S21]: Column already exists") === false) {
        throw $e;
    }
}

try {
    $installer->run("ALTER TABLE `ch_canadapost_shipment` ADD COLUMN is_delivered TINYINT(1) NOT NULL");
} catch (Exception $e) {
    // Check if we're just trying to re-run scripts and the column already exists.
    if (strpos($e->getMessage(), "SQLSTATE[42S21]: Column already exists") === false) {
        throw $e;
    }
}

try {
    $installer->run("ALTER TABLE `ch_canadapost_shipment` ADD COLUMN is_checked TINYINT(1) NOT NULL");
} catch (Exception $e) {
    // Check if we're just trying to re-run scripts and the column already exists.
    if (strpos($e->getMessage(), "SQLSTATE[42S21]: Column already exists") === false) {
        throw $e;
    }
}

$installer->endSetup();