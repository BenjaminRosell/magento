<?php

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

if ($this->getTable('ch_canadapost_quote_param') != 'ch_canadapost_quote_param') {
    try {
        $installer->run("ALTER TABLE `{$this->getTable('ch_canadapost_quote_param')}` DROP FOREIGN KEY `ch_canadapost_quote_param_ibfk_1`;");
    } catch  (Exception $e) {
        Mage::log(__FILE__ .  ' ' . $e);
    }
    try {
        $installer->run("ALTER TABLE `{$this->getTable('ch_canadapost_quote_param')}` ADD FOREIGN KEY `magento_quote_id` (`magento_quote_id`) REFERENCES  ".$this->getTable('sales_flat_quote')."(entity_id) ON DELETE CASCADE");
    } catch (Exception $e) {
        // Check if we're just trying to re-run scripts and the foreign key has already been renamed.
        if (strpos($e->getMessage(), "SQLSTATE[HY000]: General error: 1025 Error on rename") === false) {
            throw $e;
        }
    }
}

if ($this->getTable('ch_canadapost_shipment') != 'ch_canadapost_shipment') {
    try {
        $installer->run("ALTER TABLE `{$this->getTable('ch_canadapost_shipment')}` DROP FOREIGN KEY `ch_canadapost_shipment_ibfk_1`;");
    } catch  (Exception $e) {
        Mage::log(__FILE__ .  ' ' . $e);
    }
    try {
        $installer->run("ALTER TABLE `{$this->getTable('ch_canadapost_shipment')}` ADD FOREIGN KEY `magento_shipment_id` (`magento_shipment_id`) REFERENCES  ".$this->getTable('sales_flat_shipment')."(entity_id) ON DELETE CASCADE");
    } catch (Exception $e) {
        // Check if we're just trying to re-run scripts and the foreign key has already been renamed.
        if (strpos($e->getMessage(), "SQLSTATE[HY000]: General error: 1025 Error on rename") === false) {
            throw $e;
        }
    }
}

$installer->endSetup();
