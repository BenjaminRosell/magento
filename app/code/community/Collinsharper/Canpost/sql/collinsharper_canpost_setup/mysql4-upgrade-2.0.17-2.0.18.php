<?php

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

if ($this->getTable('ch_canadapost_manifest_link') != 'ch_canadapost_manifest_link') {
    try {
        $installer->run("RENAME TABLE `ch_canadapost_manifest_link` TO ".$this->getTable('ch_canadapost_manifest_link'));
    } catch (Exception $e) {
        // Check if we're just trying to re-run scripts and the table's already been renamed.
        if (strpos($e->getMessage(), "SQLSTATE[HY000]: General error: 1017 Can't find file") === false) {
            throw $e;
        }
    }
}

if ($this->getTable('ch_canadapost_shipment_link') != 'ch_canadapost_shipment_link') {
    try {
        $installer->run("RENAME TABLE `ch_canadapost_shipment_link` TO ".$this->getTable('ch_canadapost_shipment_link'));
    } catch (Exception $e) {
        // Check if we're just trying to re-run scripts and the table's already been renamed.
        if (strpos($e->getMessage(), "SQLSTATE[HY000]: General error: 1017 Can't find file") === false) {
            throw $e;
        }
    }
}

if ($this->getTable('ch_canadapost_quote_param') != 'ch_canadapost_quote_param') {
    try {
        $installer->run("RENAME TABLE `ch_canadapost_quote_param` TO ".$this->getTable('ch_canadapost_quote_param'));
    } catch (Exception $e) {
        // Check if we're just trying to re-run scripts and the table's already been renamed.
        if (strpos($e->getMessage(), "SQLSTATE[HY000]: General error: 1017 Can't find file") === false) {
            throw $e;
        }
    }
}

if ($this->getTable('ch_canadapost_return') != 'ch_canadapost_return') {
    try {
        $installer->run("RENAME TABLE `ch_canadapost_return` TO ".$this->getTable('ch_canadapost_return'));
    } catch (Exception $e) {
        // Check if we're just trying to re-run scripts and the table's already been renamed.
        if (strpos($e->getMessage(), "SQLSTATE[HY000]: General error: 1017 Can't find file") === false) {
            throw $e;
        }
    }
}

if ($this->getTable('ch_canadapost_shipment') != 'ch_canadapost_shipment') {
    try {
        $installer->run("RENAME TABLE `ch_canadapost_shipment` TO ".$this->getTable('ch_canadapost_shipment'));
    } catch (Exception $e) {
        // Check if we're just trying to re-run scripts and the table's already been renamed.
        if (strpos($e->getMessage(), "SQLSTATE[HY000]: General error: 1017 Can't find file") === false) {
            throw $e;
        }
    }
}

if ($this->getTable('ch_canadapost_manifest') != 'ch_canadapost_manifest') {
    try {
        $installer->run("RENAME TABLE `ch_canadapost_manifest` TO ".$this->getTable('ch_canadapost_manifest'));
    } catch (Exception $e) {
        // Check if we're just trying to re-run scripts and the table's already been renamed.
        if (strpos($e->getMessage(), "SQLSTATE[HY000]: General error: 1017 Can't find file") === false) {
            throw $e;
        }
    }
}

if ($this->getTable('ch_canadapost_office') != 'ch_canadapost_office') {
    try {
        $installer->run("RENAME TABLE `ch_canadapost_office` TO ".$this->getTable('ch_canadapost_office'));
    } catch (Exception $e) {
        // Check if we're just trying to re-run scripts and the table's already been renamed.
        if (strpos($e->getMessage(), "SQLSTATE[HY000]: General error: 1017 Can't find file") === false) {
            throw $e;
        }
    }
}

$installer->endSetup();
