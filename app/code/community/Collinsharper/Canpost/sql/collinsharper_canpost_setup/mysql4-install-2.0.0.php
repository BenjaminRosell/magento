<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$installer->run("DROP TABLE IF EXISTS `ch_canadapost_shipment`");

$installer->run("DROP TABLE IF EXISTS `ch_canadapost_shipment_link`");

$installer->run("CREATE TABLE `ch_canadapost_shipment` (
  `id` int(11) NOT NULL auto_increment,
  `order_id` int(10) NOT NULL,
  `shipment_id` varchar(50) NOT NULL default '',
  `status` varchar(20) NOT NULL default '',
  `tracking_pin` varchar(100) NOT NULL default '',
  `signature` tinyint(1) NOT NULL default 0,
  `magento_shipment_id` int(10) unsigned NULL,
  `manifest_id` int(11) NULL,
  PRIMARY KEY  (`id`),
  FOREIGN KEY (`magento_shipment_id`) REFERENCES sales_flat_shipment(entity_id) ON DELETE CASCADE,
  FOREIGN KEY (manifest_id) REFERENCES ch_canadapost_manifest(id) ON DELETE CASCADE
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$installer->run("CREATE TABLE `ch_canadapost_shipment_link` (
  `id` int(11) NOT NULL auto_increment,
  `cp_shipment_id` int(11) NULL,
  `rel` varchar(10) NOT NULL default '',
  `url` varchar(255) NOT NULL default '',
  `media_type` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`),
  FOREIGN KEY (cp_shipment_id) REFERENCES ch_canadapost_shipment(id) ON DELETE CASCADE) 
  ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$installer->endSetup();