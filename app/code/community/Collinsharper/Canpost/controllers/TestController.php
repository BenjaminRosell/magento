<?php

class Collinsharper_Canpost_TestController extends Mage_Core_Controller_Front_Action
{
    public function uptodateAction()
    {
        ob_start();
        Mage::helper('chcanpost2module/rest_pdf')->load("http://collinsharper.com/moduletest/canpost/uptodate.php", 'application/pdf', '', 0);
        $returnString = ob_get_contents();
        ob_end_clean();

        header("HTTP/1.0 200 OK");

        echo $returnString;

        exit;
    }
}
