<?php
/** 
 *
 * @category    Collinsharper
 * @package     Collinsharper_Canpost
 * @author      Maxim Nulman
 */
class Collinsharper_Canpost_Adminhtml_ShippingController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/manifest');
    }
    
    public function getServicesAction()
    {
        
        $quote = Mage::getSingleton('adminhtml/sales_order_create')->getQuote();
        
        $shipping_method = $quote->getShippingAddress()->getShippingMethod();
        
        if (preg_match('/^chcanpost2module_(.+)/', $shipping_method, $matches)) {
            
            $params = Mage::getModel('chcanpost2module/quote_param')->getParamsByQuote($quote->getId());
            
        }
        
        Mage::getModel('chcanpost2module/quote_param')->resetParams($quote->getId());
        
        $service_code = $this->getRequest()->getParam('service_code');
        
        $data = $this->getServicesOptions($quote, $service_code);
        
        $data['saved_params'] = array();
        
        if (!empty($params)) {
            
            Mage::getModel('chcanpost2module/quote_param')->setParams($quote->getId(), $params);
            
            $options = array(
                            'SO' => $params['signature'],
                            'COV' => $params['coverage'],
                            'HFP' => $params['card_for_pickup'],
                            'DNS' => $params['do_not_safe_drop'],
                            'LAD' => $params['leave_at_door'],
                            'D2PO' => (int)!empty($params['office_id']),
                            'office' => (int)$params['office_id'],
                );

            if (!empty($params['office_id'])) {
                
                $office = Mage::getModel('chcanpost2module/office')->load($params['office_id']);
                
                if ($office->getId()) {
                    
                    $options['office_name'] = $office->getCpOfficeName();
                    
                    $options['office_address'] = $office->getOfficeAddress();
                    
                }
                
            }
            
            $data['saved_params'] = $options;
            
        }
        
        header("HTTP/1.0 200 OK");

        header('Content-type: application/json');
        
        echo json_encode($data);
        
        exit;
        
    }
    
    
    public function returnAction()
    {

        $shipmentId = $this->getRequest()->getParam('shipment_id');
        
        $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);

        if ($shipment->getId()) {
            
            $shipping_address = Mage::getModel('sales/order_address')->load($shipment->getShippingAddressId()); 
            
            $response = Mage::helper('chcanpost2module/rest_return')->create($shipping_address);
            
            $xml = new SimpleXMLElement($response);    

            if (!empty($xml->{'tracking-pin'}) && !empty($xml->links->link['href'])) {
                
                $return = Mage::getModel('chcanpost2module/return')->setOrderId($shipment->getOrderId())
                                                                   ->setShipmentId($shipment->getId())
                                                                   ->setStatus('created')
                                                                   ->setTrackingPin($xml->{'tracking-pin'})
                                                                   ->setLink($xml->links->link['href'])
                                                                   ->save();

                $track = Mage::getModel('sales/order_shipment_track')->addData(array(
                    'carrier_code' => 'chcanpost2module',
                    'title' => Mage::helper('chcanpost2module')->__('Return for order #').$shipment->getIncrementId(),
                    'number' => $xml->{'tracking-pin'},
                ));

                $shipment->addTrack($track)->save();

                $return_label_url = Mage::getModel('core/url')
                        ->setStore($shipment->getStoreId())
                        ->getUrl('canpost/shipping/getReturnLabel', array('order_id' => $shipment->getOrderId()));

                $customer_email = $shipping_address->getEmail();
                
                if (empty($customer_email)) {
                    
                    $customer_email = $shipping_address->getOrder()->getCustomerEmail();
                    
                }

                Mage::helper('chcanpost2module/email')->sendReturn($customer_email, $shipping_address->getFirstname(), $return_label_url, $shipment);
                
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('chcanpost2module')->__('Canada Post Return Label has been sent to the customer'));           
                
            } else {
                
                Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Return can not be sent'));
                
                if (!empty($xml->message->description)) {
                    
                    Mage::helper('chcanpost2module')->log((string)$xml->message->description);
                    
                }
                
            }
        
            if (!empty($_SERVER['HTTP_REFERER'])) {

                $this->_redirectUrl($_SERVER['HTTP_REFERER']);

            } else {

                $this->_redirect('/');

            }
            
        }
        
    }
    
    
    public function labelAction()
    {

	$this->getRequest()->setParam('shipment_ids', array($this->getRequest()->getParam('shipment_id')));

	$this->massLabelAction();
        
    }
    
    
    public function massLabelAction()
    {
        
        $ids = $this->getRequest()->getParam('shipment_ids');
        
        if (!empty($ids) && is_array($ids)) {
           
            $pdf = new Zend_Pdf;
            
            $print = false;
            
            foreach ($ids as $shipmentId) {    
                
                $order_id = Mage::getModel('sales/order_shipment')->load($shipmentId)->getOrderId(); 
                
                $labels = Mage::getModel('chcanpost2module/link')->getLabelDataByOrderId($order_id);   
                
		foreach ($labels as $label_data) {

			if (!empty($label_data['url']) && !empty($label_data['media_type'])) {
			
			    ob_start();

			    Mage::helper('chcanpost2module/rest_pdf')->load($label_data['url'], $label_data['media_type'], '', 0);

			    $pdfString = ob_get_contents();

			    ob_end_clean();
			    
			    if (!empty($pdfString)) {

				try {
				
				    Mage::helper('chcanpost2module/rest_pdf')->addPage($pdf, $pdfString);

				    $print = true;
				
				} catch (Exception $e) {
				    
				    Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Error: '.$e->getMessage()));
				    
				}

			    }
			
			}

		}
			
		$invoice_data_list = Mage::getModel('chcanpost2module/link')->getLabelDataByOrderId($order_id, 'commercial');  
	       
		if (!empty($invoice_data_list)) {

			foreach ($invoice_data_list as $invoice_data) {

				if (!empty($invoice_data['url']) && !empty($invoice_data['media_type'])) {

				    ob_start();

				    Mage::helper('chcanpost2module/rest_pdf')->load($invoice_data['url'], $invoice_data['media_type'], '', 0);

				    $pdfString = ob_get_contents();

				    ob_end_clean();

				    if (!empty($pdfString)) {

					Mage::helper('chcanpost2module/rest_pdf')->addPage($pdf, $pdfString);

					$print = true;

				    }

				}

			}

		}
 
            }
            
            if ($print) {
            
        header("HTTP/1.0 200 OK");

                header('content-type: application/pdf');

                header('Content-Disposition: attachment; filename="labels-'.date('Y-m-d--H-i-s').'.pdf"');  

                echo $pdf->render();
            
            } else {
                
                Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Labels can not be retrieved'));
                
                if (!empty($_SERVER['HTTP_REFERER'])) {
                
                    $this->_redirectUrl($_SERVER['HTTP_REFERER']);
                
                } else {
                    
                    $this->_redirect('/');
                    
                }
                
            }
            
        } else {
            
            $this->_redirect('/');
            
        }
        
    }
    
    
    public function getRatesAction()
    {

        $coverage = $this->getRequest()->getParam('coverage', 0);

        $signature = $this->getRequest()->getParam('signature', 0);

        $card_for_pickup = $this->getRequest()->getParam('card_for_pickup', 0);

        $do_not_safe_drop = $this->getRequest()->getParam('do_not_safe_drop', 0);

        $leave_at_door = $this->getRequest()->getParam('leave_at_door', 0);

        $cod = $this->getRequest()->getParam('cod', 0);

        $office_id = $this->getRequest()->getParam('office_id', null);
        
        $deliver_to_post_office = $this->getRequest()->getParam('deliver_to_post_office', 0);

        $service_code = str_replace('chcanpost2module_', '', $this->getRequest()->getParam('service_code', ''));
        
        Mage::getSingleton('checkout/session')->setServiceCode($service_code);
        
        $quote = Mage::getSingleton('adminhtml/sales_order_create')->getQuote();

        $total = $quote->getGrandTotal();

        $shipping_address = $quote->getShippingAddress();
        
        $available_options = Mage::helper('chcanpost2module/option')->getAvailableOptions($service_code, $shipping_address->getCountryId());
        
        if ($coverage) {

            $max_coverage = Mage::helper('chcanpost2module/option')->getMaxCoverage($service_code, $shipping_address->getCountryId());
            
            $coverage_amount = ($total < $max_coverage) ? $total : $max_coverage;

        } else {
            
            $coverage_amount = 0;
            
        }
        
        $conflicted_options = Mage::helper('chcanpost2module/option')->getConflicetedOptions(
                array(
                    'signature' => $signature,
                    'coverage' => $coverage,
                    'card_for_pickup' => $card_for_pickup,
                    'do_not_safe_drop' => $do_not_safe_drop,
                    'leave_at_door' => $leave_at_door,
                    'cod' => $cod,
                    'office_id' => $office_id,
                )
                );

        Mage::helper('chcanpost2module/office')->updateShippingAddress($quote, $deliver_to_post_office, $office_id);
       
        $options = array();

        if (!empty($service_code)) {
        
            $params = Mage::getModel('chcanpost2module/quote_param')->updateForQuote(
                        $quote->getId(),
                        $signature,
                        (int)!empty($coverage),
                        $coverage_amount,
                        (int)!empty($card_for_pickup),
                        (int)!empty($do_not_safe_drop),
                        (int)!empty($leave_at_door),
                        (int)!empty($cod),
                        $office_id
                        );            
            
            $options = array(
                            'SO' => $params->getSignature(),
                            'COV' => $params->getCoverage(),
                            'COD' => $params->getCod(),
                            'HFP' => $params->getCardForPickup(),
                            'DNS' => $params->getDoNotSafeDrop(),
                            'LAD' => $params->getLeaveAtDoor(),
                            'D2PO' => (int)!empty($office_id),
                            'office' => (int)$office_id,
                );
            
            foreach ($options as $code => $value) {
                
                if (empty($available_options[$code]) && !in_array($code, $conflicted_options)) {
                    
                    $conflicted_options[] = $code;
                    
                }                 
                
            }

            if (!empty($office_id)) {
                
                $office = Mage::getModel('chcanpost2module/office')->load($office_id);
                
                if ($office->getId()) {
                    
                    $options['office_name'] = $office->getCpOfficeName();
                    
                    $options['office_address'] = $office->getOfficeAddress();
                    
                }
                
            }
            
        }
        
        $shipping_address->setCollectShippingRates(true)->collectShippingRates()->save();

        $groups = $shipping_address->getGroupedAllShippingRates();

        $new_rates = array();
        
        foreach ($groups as $code => $rates) {
            
            if ($code == 'chcanpost2module') {
            
                foreach ($rates as $rate) {

                    $new_rates[$rate->getMethod()] = array(
                        'title' => $rate->getMethodTitle(),
                        'price' => Mage::helper('core')->currency($rate->getPrice())
                    );
                    
                }
            
            }
            
        }
        
        $data = array(
            'rates' => $new_rates,
            'conflicted_options' => array_values($conflicted_options),
            'params' => $options,
        );
        
        header("HTTP/1.0 200 OK");

        header('content-type: application/json');
        
        echo json_encode($data);
        
    }
    
    
    public function getNearestOfficeAction()
    {
        
        $postal_code = $this->getRequest()->getParam('postcode', 0);
        
        if (empty($postal_code)) {
        
            $postal_code = Mage::getSingleton('adminhtml/sales_order_create')->getQuote()->getShippingAddress()->getPostcode();
        
        }
        
        $response = Mage::helper('chcanpost2module/rest_office')->getNearest($postal_code);

        $options = array();
        
        if (!empty($response)) {
            
            $xml = new SimpleXMLElement($response);           
            
            foreach ($xml->{'post-office'} as $office) {
                
                $cp_office = Mage::getModel('chcanpost2module/office')->getByCpOfficeId($office->{'office-id'});
                
                if (!$cp_office->getId()) {
                    
                    $cp_office->setCity($office->address->city)
                              ->setPostalCode($office->address->{'postal-code'})
                              ->setProvince($office->address->province)
                              ->setAddress($office->address->{'office-address'})
                              ->setLocation($office->location)
                              ->setLink($office->link['href'])
                              ->setMediaType($office->link['media-type'])
                              ->setCpOfficeId($office->{'office-id'})
                              ->setCpOfficeName($office->name)
                              ->setBilingual($office->{'bilingual-designation'})
                              ->setCraetedAt(date('Y-m-d H:i:s'))
                              ->save();
                    
                }
                
                $options[$cp_office->getId()] = array(
                    'name' => $cp_office->getName().' ('.$cp_office->getCity().' '.$cp_office->getAddress().')',
                    'selected' => (Mage::getSingleton('checkout/session')->getOfficeId() == $cp_office->getId())
                    );
                
            }
            
        }
        
        header("HTTP/1.0 200 OK");

        header('content-type: application/json');
        
        echo json_encode($options);
        
    }
    
    
    public function approveAction() {
        
        //die("OK");
        
    }
    
    
    public function getInitStateAction() {
        
        $quote = Mage::getSingleton('adminhtml/sales_order_create')->getQuote();
            
        $params = Mage::getModel('chcanpost2module/quote_param')->getParamsByQuote($quote->getId());

        $data['saved_params'] = array(
                                    'SO' => $params['signature'],
                                    'COV' => $params['coverage'],
                                    'HFP' => $params['card_for_pickup'],
                                    'DNS' => $params['do_not_safe_drop'],
                                    'LAD' => $params['leave_at_door'],
                                    'COD' => $params['cod'],
                                    'D2PO' => (int)!empty($params['office_id']),
                                    'office' => (int)$params['office_id'],
                                );           
        
        header("HTTP/1.0 200 OK");

        header('content-type: application/json');
        
        echo json_encode($data);
        
    }
    
    
    private function getServicesOptions($quote, $service_code = null) {
        
        $data = array();

        if (preg_match('/chcanpost2module_(.+)/', $service_code, $matches)) {
            
            $services = Mage::helper('chcanpost2module/rest_service')->getInfo($matches[1], $quote->getShippingAddress()->getCountry());
        
            $data['exclude_services'] = array();
            
            $available_options = array('SO', 'COV', 'HFP', 'DNS', 'LAD', 'COD', 'D2PO');
            
            if (!empty($services->options->option)) {
                
                $options = array();
                
                foreach ($services->options->option as $opt) {

                    $options[] = (string)$opt->{'option-code'};
                    
                }
                
                foreach ($available_options as $opt) {
                    
                    if (!in_array($opt, $options)) {
                        
                        $data['exclude_services'][] = $opt;
                        
                    }
                    
                }
                
            }
            
        }
        
        return $data;
        
    }
    
}
