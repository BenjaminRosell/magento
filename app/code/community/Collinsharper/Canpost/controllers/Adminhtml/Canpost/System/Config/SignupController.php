<?php

class Collinsharper_Canpost_Adminhtml_Canpost_System_Config_SignupController
    extends Mage_Adminhtml_Controller_Action
{
    public function updateAction()
    {
        try {
        Mage::helper('chcanpost2module')->log(__CLASS__ . __LINE__ );
        $postData = array();
        $url = Mage::getStoreConfig('carriers/chcanpost2module/signup_url');
        $help = Mage::getHelper('chcanpost2module/rest_registration');
        $this->_getSession()->setCanadapostRegistrationToken(false);
        $postData['token'] = $help->getRegistrationToken();
        Mage::helper('chcanpost2module')->log(__CLASS__ . __LINE__ . " we have ".  $postData['token']);
        if(!$postData['token'])
        {
            $this->_getSession()->addError(Mage::helper('adminhtml')->__('Unable to get registration token. Please Try again later.'));
            $this->_redirect('../../../system_config/edit/section/carriers');
            return;
        }
        Mage::helper('chcanpost2module')->log(__CLASS__ . __LINE__ );
        $this->_getSession()->setCanadapostRegistrationToken($postData['token']);
     //   header("Location: ".$url,TRUE,302);
//        return-url *
//(for call-back to your system)
//token-id
//(to refer to the interested user)
//platform-id
//(your customer number)
//first-name
//last-name
//address-line-1
//prov-state
//postal-zip-code
//country-code
//email
//city

	        $urlPar = array();
        
        $store = $this->getRequest()->getParam('store', '');

        $website = $this->getRequest()->getParam('website', '');

        if (!empty($store)) {

                $urlPar['store'] = $store;

        }

        if (!empty($website)) {

                $urlPar['website'] = $website;

        }

        $postData['return-url']= Mage::getSingleton('adminhtml/url')->getUrl('canpost/signup/return', $urlPar);
        // $postData['token']
        $postData['platform-id']= 99;
        // this isnt great
        // needs to be abstracted

        $formdata = '';
        foreach($postData as $k => $v)
        {
            $formdata .= "<input type=\"hidden\" value=\"{$v}\"  name=\"{$k}\" />\n";
        }
        echo <<<EOD
        <form name="canadapost-signup" id="canadapost-signup" method="POST" action="{$url}"?
         {$formdata}
         <input type="submit" >
        </form>
        <script>
         document.getElementById('canadapost-signup').submit();
        </script>
EOD;

      //  exit;
    } catch (exception $e)
        {
            Mage::helper('chcanpost2module')->log(__FILE__ . " EXCEPTION " . $e->getMessage());
        }

    }

}
