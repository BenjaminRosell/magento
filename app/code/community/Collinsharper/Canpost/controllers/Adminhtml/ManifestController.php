<?php
/** 
 *
 * @category    Collinsharper
 * @package     Collinsharper_Canpost
 * @author      Maxim Nulman
 */
class Collinsharper_Canpost_Adminhtml_ManifestController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/manifest');
    }

    public function indexAction()
    {
        
        $this->loadLayout();

        $block = $this->getLayout()->createBlock(
            'Collinsharper_Canpost_Block_Adminhtml_Manifest',
            'manifest'
        );
        
        $this->_title('Manage Manifests');
        
        $this->getLayout()->getBlock('content')->append($block);
        
        $this->renderLayout();
        
    }
    
    
    public function createAction()
    {

        $this->_title(Mage::helper('chcanpost2module')->__('Manifest'))
             ->_title(Mage::helper('chcanpost2module')->__('Create Manifest'));

        $this->loadLayout();

        $block = $this->getLayout()->createBlock(
            'Collinsharper_Canpost_Block_Adminhtml_Manifest_Shipment',
            'manifest'
        );
        
        $this->getLayout()->getBlock('content')->append($block);
        
        $this->renderLayout();
        
    }
    
    
    public function viewAction()
    {

        $this->_title(Mage::helper('chcanpost2module')->__('Manifest'))
             ->_title(Mage::helper('chcanpost2module')->__('View Manifest'));
        
        $this->loadLayout();

        $block = $this->getLayout()->createBlock(
            'Collinsharper_Canpost_Block_Adminhtml_Manifest_Shipment',
            'manifest'
        );  
        
        $this->getLayout()->getBlock('content')->append($block);
        
        $this->renderLayout();
        
    }
    
    
    public function massCreateAction()
    {
        
        $shipment_ids = $this->getRequest()->getParam('shipment_ids');
        
        $added = 0;
        
        if (!empty($shipment_ids) && is_array($shipment_ids)) {

	    if (Mage::getStoreConfig('carriers/chcanpost2module/scope') == 0) {	

		    $collection = Mage::getModel('sales/order_shipment')->getCollection()->addFieldToFilter('entity_id', array('IN' => $shipment_ids));

		    $collection->distinct(true);

		    $collection->getSelect()->reset(Zend_Db_Select::COLUMNS)->columns('store_id');

		    $good_to_add = ($collection->count() == 1);

	    } else {

		$good_to_add = true;

	    }

	    if ($good_to_add) {
            
		    $manifest = Mage::getModel('chcanpost2module/manifest')
			    ->setGroupId(time())
			    ->setStatus('pending')
			    ->setCreatedAt(date('Y-m-d H:i:s'))
			    ->setUpdatedAt(date('Y-m-d H:i:s'))
			    ->save();            
		    
		    $added = $this->updateShipments($manifest->getId(), $manifest->getGroupId(), $shipment_ids, 'new');
		    
		    if ($added > 0) {
		    
			Mage::getSingleton('core/session')->addSuccess(Mage::helper('chcanpost2module')->__('%s of %s Shipment has been successfully created', $added, count($shipment_ids)));
		    
		    } else {
			
			$manifest->delete();
			
		    }

		    $this->_redirect('*/*/index');

            } else {

		Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Only shipments from one store can be added to manifest'));		

		$this->_redirect('*/*/create');

	    }
            
        }
        
    }
    
    
    public function massAddAction()
    {
        
        $manifest_id = $this->getRequest()->getParam('manifest_id'); 

        $shipment_ids = $this->getRequest()->getParam('shipment_ids');
        
        if (!empty($shipment_ids) && is_array($shipment_ids) && !empty($manifest_id)) {
     
            $manifest = Mage::getModel('chcanpost2module/manifest')->load($manifest_id);
            
            $added = $this->updateShipments($manifest->getId(), $manifest->getGroupId(), $shipment_ids, 'add');
            
            if ($added > 0) {
            
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('chcanpost2module')->__('%s of %s Shipment has been successfully added', $added, count($shipment_ids)));
            
            }
            
            $this->_redirect('*/*/view', array('manifest_id' => $manifest_id));
            
        }
        
    }
    
    
    public function massRemoveAction()
    {
        
        $manifest_id = $this->getRequest()->getParam('manifest_id'); 

        $shipment_ids = $this->getRequest()->getParam('shipment_ids');

        if (!empty($shipment_ids) && is_array($shipment_ids) && !empty($manifest_id)) {
     
            $manifest = Mage::getModel('chcanpost2module/manifest')->load($manifest_id);
            
            $removed = $this->updateShipments($manifest->getId(), $manifest->getGroupId(), $shipment_ids, 'remove');
            
            if ($removed > 0) {
            
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('chcanpost2module')->__('%s of %s Shipment has been successfully removed', $removed, count($shipment_ids)));
            
            }
            
            $shipments_in_manifest = Mage::getModel('chcanpost2module/shipment')->getCollection()
                                ->addFieldToFilter('manifest_id', $manifest_id)
                                ->getSize();
            
            if (empty($shipments_in_manifest)) {
                
                $manifest->delete();
                
                $this->_redirect('*/*/index');
                
            } else {
            
                $this->_redirect('*/*/view', array('manifest_id' => $manifest_id));
                
            }
            
        }
        
    }
    
    
    public function massTransmitAction()
    {

        $manifest_ids = $this->getRequest()->getParam('manifest_ids');
        
        if (!empty($manifest_ids) && is_array($manifest_ids)) {
            
            $transmited = 0;
            
            foreach ($manifest_ids as $manifest_id) {

		$manifest = Mage::getModel('chcanpost2module/manifest')->load($manifest_id);                

                if ($manifest->getId()) {

  		    $cp_shipment = Mage::getModel('chcanpost2module/shipment')->getCollection()->addFieldToFilter('manifest_id', $manifest_id)->getFirstItem();

		    $shipment = Mage::getModel('sales/order_shipment')->load($cp_shipment->getMagentoShipmentId());

                    $response = Mage::helper('chcanpost2module/rest_transmit')->transmit($manifest, $shipment->getStoreId());

                    $xml = new SimpleXMLElement($response);

                    if (count($xml->link) > 0) {
                        
                        foreach ($xml->link as $link) {

                            if (!empty($link['rel']) && $link['rel'] == 'manifest') {

                                Mage::getModel('chcanpost2module/manifestlink')
                                        ->setManifestId($manifest_id)
                                        ->setLink($link['href'])
                                        ->save();


                            }

                        }
                        
                        $manifest->setUrl('transmitted')
                                 ->setStatus('transmitted')
                                 ->setUpdatedAt(date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp(time())))
                                 ->save();
                        
                        $transmited++; 

                    } else {

                        Mage::helper('chcanpost2module')->log("canada post transmit error: ".$response);

                        Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Transmission Error'));

                    }

                } else {

                    Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Shipment not found'));

                }
            
            }
            
            if ($transmited > 0) {
            
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('chcanpost2module')->__('%s of %s Manifests have been successfully transmitted',$transmited ,count($manifest_ids)));
            
            }

            $this->_redirect('*/*/index');
            
        }
        
    }
    
    
    /**
     * Join all manifest documnts in one
     */
    public function printAction()
    {

        $manifest_id = $this->getRequest()->getParam('manifest_id');
        
        $links = Mage::getModel('chcanpost2module/manifestlink')->getCollection()
            ->addFieldToFilter('manifest_id', $manifest_id);
        
        if ($links->getSize() > 0) {
            
            $pdf = new Zend_Pdf;
            
            $print = false;
            
            foreach ($links as $manifest) {
                
                // TODO: Add a media_type column to store the manifest link's media type returned from the API
                $response = Mage::helper('chcanpost2module/rest_manifest')->getManifest($manifest->getLink(), $manifest->getMediaType());

                $xml = new SimpleXMLElement($response);

                foreach ($xml->links->link as $link) {

                    if ($link['rel'] == 'artifact') {

                        ob_start();

                        Mage::helper('chcanpost2module/rest_manifest')->getPdf($link['href'], 0);

                        $pdfString = ob_get_contents();

                        ob_end_clean();

                        if (!empty($pdfString)) {

                            $extractor = new Zend_Pdf_Resource_Extractor();

                            $temp_pdf = Zend_Pdf::parse($pdfString);

                            for ($i = 0; $i < count($temp_pdf->pages); $i++) {
                                
                                $page = $extractor->clonePage($temp_pdf->pages[$i]);

                                $pdf->pages[] = $page;
                                
                            }

                            $print = true;

                        }

                    }

                }
            
            }
            
            if ($print) {
                
                header('content-type: application/pdf');

                header('Content-Disposition: attachment; filename="manifests-'.date('Y-m-d--H-i-s').'.pdf"');  

                echo $pdf->render();
                
            } else {
                
                Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Manifest can not be retreived'));

                if (!empty($_SERVER['HTTP_REFERER'])) {

                    $this->_redirectUrl($_SERVER['HTTP_REFERER']);

                } else {

                    $this->_redirect('/');

                }
                
            }
        
        } else {
            
            Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Manifest can not be retreived'));
            
            $this->_redirectUrl($_SERVER['HTTP_REFERER']);
            
        }
        
    }    
    
    
    private function updateShipments($manifest_id, $group_id, $shipment_ids, $action)
    {
        
        $processed = 0;
        
        foreach ($shipment_ids as $shipmentId) {

            $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);

            $cp_shipment = Mage::getModel('chcanpost2module/shipment')->getShipmentById($shipmentId);
            
            if ($shipment->getId()) {

                switch ($action) {
                    
                    case 'new':
                    case 'add': 
                
                        if ($cp_shipment->getManifestId() != $manifest_id) {

                            $shipmentSuccess = false;
                            try{
                                $shipmentSuccess = $shipment->createCpShipment($group_id, $manifest_id, $shipmentId);
                            } catch (Exception $e) {
                                $exceptionMessage = $e->getMessage();
                            }

                            if (!$shipmentSuccess) {
                                $shipmentErrorMessage = isset($exceptionMessage) ? $exceptionMessage : $shipment->getError();

                                Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Canada Post Shipment for shipment #%s has not been created', $shipment->getId()));

                                Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Error: ') . $shipmentErrorMessage);

                                Mage::helper('chcanpost2module')->log($shipmentErrorMessage);



                            } else {

                                $shipment->setManifestId($manifest_id)->save();

                                $processed++;

                            }
                        
                        }
                        
                        break;
                        
                    case 'remove':

                        if ($cp_shipment->getManifestId() == $manifest_id) {

                            if ($shipment->removeCpShipment($cp_shipment)) {

                                $processed++;
                            
                            }
                        
                        }
                        
                        break;
                
                }

            } else {

                Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Shipment not found'));

            }

        }
        
        return $processed;
        
    }
    
}
