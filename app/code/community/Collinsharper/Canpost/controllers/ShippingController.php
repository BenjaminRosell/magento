<?php
/**
 *
 * @category    Collinsharper
 * @package     Collinsharper_Canpost
 * @author      Maxim Nulman
 */
class Collinsharper_Canpost_ShippingController extends Mage_Core_Controller_Front_Action
{


    public function getServicesAction()
    {

        $quote = Mage::getSingleton('checkout/session')->getQuote();

        $shipping_method = $quote->getShippingAddress()->getShippingMethod();

        if (preg_match('/^chcanpost2module_(.+)/', $shipping_method, $matches)) {

            $shipping_method = $matches[1];

            $params = Mage::getModel('chcanpost2module/quote_param')->getParamsByQuote($quote->getId());

        }

        Mage::getModel('chcanpost2module/quote_param')->resetParams($quote->getId());

        $data = array();

        $service_code = $this->getRequest()->getParam('service_code');

        $postal_code = $this->getRequest()->getParam('postal_code');

        if (preg_match('/chcanpost2module_(.+)/', $service_code, $matches)) {

            $services = Mage::helper('chcanpost2module/rest_service')->getInfo($matches[1], Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getCountry());

            $data['exclude_services'] = array();

            $data['selected_options'] = array();

            $available_options = array('SO', 'COV', 'COD', 'HFP', 'DNS', 'LAD', 'D2PO');

            if (!empty($services->options->option)) {

                $options = array();

                foreach ($services->options->option as $opt) {

                    $options[] = (string)$opt->{'option-code'};

                }

                foreach ($available_options as $opt) {

                    if (!in_array($opt, $options)) {

                        $data['exclude_services'][] = $opt;

                    }

                }

                $data['readonly_options'] = array();

                if (Mage::getStoreConfig('carriers/chcanpost2module/require_signature')
                    && Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal() > Mage::getStoreConfig('carriers/chcanpost2module/signature_threshhold')) {

                    if (!in_array('SO', $data['exclude_services'])) {

                        Mage::getSingleton('checkout/session')->setSignature(1);

                        $data['readonly_options'][] = 'SO';

                        $data['selected_options'][] = 'SO';

                    } else {

                        Mage::getSingleton('checkout/session')->setSignature(0);

                    }

                }

            }

        }

        $data['exclude_services'] = array_values($data['exclude_services']);

        $data['selected_options'] = array_values($data['selected_options']);

        $data['readonly_options'] = array_values($data['readonly_options']);

        $data['saved_params'] = array();

        if (!empty($params)) {

            Mage::getModel('chcanpost2module/quote_param')->setParams($quote->getId(), $params);

            $options = array(
                'SO' => $params['signature'],
                'COV' => $params['coverage'],
                'HFP' => $params['card_for_pickup'],
                'DNS' => $params['do_not_safe_drop'],
                'LAD' => $params['leave_at_door'],
                'COD' => $params['cod'],
                'D2PO' => (int)!empty($params['office_id']),
                'office' => (int)$params['office_id'],
            );

            if (!empty($params['office_id'])) {

                $office = Mage::getModel('chcanpost2module/office')->load($params['office_id']);

                if ($office->getId()) {

                    $options['office_name'] = $office->getCpOfficeName();

                    $options['office_address'] = $office->getOfficeAddress();

                }

            }

            $data['saved_params'] = $options;

        }

        header("HTTP/1.0 200 OK");

        header('Content-type: application/json');

        echo json_encode($data);

        exit;

    }


    public function getNearestOfficeAction()
    {

        $postal_code = $this->getRequest()->getParam('postal_code');

        $options = Mage::helper('chcanpost2module')->getNearestOffices($postal_code);

        header('content-type: application/json');

        echo json_encode($options);

        exit;

    }


    public function getRatesAction()
    {

        $coverage = $this->getRequest()->getParam('coverage', 0);

        $signature = $this->getRequest()->getParam('signature', 0);

        $card_for_pickup = $this->getRequest()->getParam('card_for_pickup', 0);

        $do_not_safe_drop = $this->getRequest()->getParam('do_not_safe_drop', 0);

        $cod = $this->getRequest()->getParam('cod', 0);
        $leave_at_door = $this->getRequest()->getParam('leave_at_door', 0);

        $deliver_to_post_office = $this->getRequest()->getParam('deliver_to_post_office', 0);

        $office_id = $this->getRequest()->getParam('office_id', null);

        $service_code = str_replace('chcanpost2module_', '', $this->getRequest()->getParam('service_code', ''));

        $quote = Mage::getSingleton('checkout/session')->getQuote();

        $total = $quote->getGrandTotal();

        // KL: We don't need to set Service Code again as it is already saved
        //Mage::getSingleton('checkout/session')->setServiceCode($service_code);

        if (!$deliver_to_post_office || empty($office_id)) {

            $office_id = null;

        }

        $shipping_address = $quote->getShippingAddress();

        $available_options = Mage::helper('chcanpost2module/option')->getAvailableOptions($service_code, $shipping_address->getCountryId());


        if ($coverage && !empty($available_options['COV']['max'])) {

            $max_coverage = $available_options['COV']['max'];

            $coverage_amount = ($total < $max_coverage) ? $total : $max_coverage;

        } else {

            $coverage_amount = 0;

        }

        $conflicted_options = Mage::helper('chcanpost2module/option')->getConflicetedOptions(
            array(
                'signature' => $signature,
                'coverage' => $coverage,
                'card_for_pickup' => $card_for_pickup,
                'do_not_safe_drop' => $do_not_safe_drop,
                'cod' => $cod,
                'leave_at_door' => $leave_at_door,
                'office_id' => $office_id,
            )
        );

        $selected_options = array();

        $readonly_options = array();

        if (Mage::getStoreConfig('carriers/chcanpost2module/require_signature')
            && Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal() > Mage::getStoreConfig('carriers/chcanpost2module/signature_threshhold')) {

            if (!empty($available_options['SO']) && !in_array('SO', $conflicted_options)) {

                $signature = 1;

                $readonly_options[] = 'SO';

                $selected_options[] = 'SO';

            } else {

                $signature = 0;

            }

        } else {

            $signature = (int)!empty($signature);

        }

        if (!empty($service_code)) {

            $params = Mage::getModel('chcanpost2module/quote_param')->updateForQuote(
                $quote->getId(),
                $signature,
                (int)!empty($coverage),
                $coverage_amount,
                (int)!empty($card_for_pickup),
                (int)!empty($do_not_safe_drop),
                (int)!empty($leave_at_door),
                (int)!empty($cod),
                $office_id
            );

            $options = array(
                'SO' => $params->getSignature(),
                'COV' => $params->getCoverage(),
                'HFP' => $params->getCardForPickup(),
                'DNS' => $params->getDoNotSafeDrop(),
                'COD' => $params->getCod(),
                'LAD' => $params->getLeaveAtDoor(),
                'D2PO' => (int)!empty($office_id),
                'office' => (int)$office_id,
            );

            if (!empty($office_id)) {

                $office = Mage::getModel('chcanpost2module/office')->load($office_id);

                if ($office->getId()) {

                    $options['office_name'] = $office->getCpOfficeName();

                    $options['office_address'] = $office->getOfficeAddress();

                }

            }

        }

        $shipping_address->setCollectShippingRates(true)->collectShippingRates()->save();
        $groups = $shipping_address->getGroupedAllShippingRates();

        $new_rates = array();

        foreach ($groups as $code => $rates) {

            if ($code == 'chcanpost2module') {

                foreach ($rates as $rate) {

                    $new_rates[$rate->getMethod()] = array(
                        'title' => $rate->getMethodTitle(),
                        'price' => Mage::helper('core')->formatPrice($rate->getPrice(), true),
                        'est_delivery_date' => $rate->getEstDeliveryDate(),
                    );

                }

            }

        }

        $data = array(
            'rates' => $new_rates,
            'conflicted_options' => array_values($conflicted_options),
            'selected_options' => array_values($selected_options),
            'readonly_options' => array_values($readonly_options),
            'params' => (!empty($options)) ? $options : array(),
        );

        header("HTTP/1.0 200 OK");

        header('content-type: application/json');

        echo json_encode($data);

        exit;

    }


    public function getReturnLabelAction()
    {

        $order_id = $this->getRequest()->getParam('order_id', 0);

        $returns = Mage::getModel('chcanpost2module/return')->getCollection()->addFieldToFilter('order_id', $order_id);

        if ($returns->getSize() > 0) {

            $pdf = new Zend_Pdf;

            $print = false;

            foreach ($returns as $return) {

                ob_start();

                Mage::helper('chcanpost2module/rest_return')->getLabel($return['link']);

                $pdfString = ob_get_contents();

                ob_end_clean();

                if (!empty($pdfString)) {

                    $extractor = new Zend_Pdf_Resource_Extractor();

                    $temp_pdf = Zend_Pdf::parse($pdfString);

                    $page = $extractor->clonePage($temp_pdf->pages[0]);

                    $pdf->pages[] = $page;

                    $print = true;

                }

            }

            if ($print) {

                header('content-type: application/pdf');

                header('Content-Disposition: attachment; filename="return-labels-'.date('Y-m-d--H-i-s').'.pdf"');

                echo $pdf->render();

            } else {

                Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Labels can not be retrieved'));

                if (!empty($_SERVER['HTTP_REFERER'])) {

                    $this->_redirectUrl($_SERVER['HTTP_REFERER']);

                } else {

                    $this->_redirect('/');

                }

            }

        } else {

            Mage::getSingleton('core/session')->addError(Mage::helper('chcanpost2module')->__('Labels can not be retrieved'));

            if (!empty($_SERVER['HTTP_REFERER'])) {

                $this->_redirectUrl($_SERVER['HTTP_REFERER']);

            } else {

                $this->_redirect('/');

            }

        }

    }


    public function saveInfoAction()
    {

        $postcode = $this->getRequest()->getParam('postcode');

        if (!empty($postcode)) {

            Mage::getSingleton('checkout/session')->setPostalCode($postcode);

        }

    }


    public function getCpOfficeDetailsAction()
    {

        header('content-type: application/json');

        $office_id = $this->getRequest()->getParam('office_id', 0);

        if (!empty($office_id)) {

            $cp_office = Mage::getModel('chcanpost2module/office')->load($office_id);

            if ($cp_office->getId()) {

                $days = array(
                    1 => 'Mon',
                    2 => 'Tue',
                    3 => 'Wed',
                    4 => 'Thu',
                    5 => 'Fri',
                    6 => 'Sat',
                    7 => 'Sun',
                );

                $response = Mage::helper('chcanpost2module/rest_office')->getDetails($cp_office->getLink());

                $xml = new SimpleXMLElement($response);

                $data = array();

                if (!empty($xml->{'hours-list'})) {

                    foreach ($xml->{'hours-list'} as $day) {

                        $data[] = array(
                            'day' => $days[(int)$day->day],
                            'from' => (string)$day->time[0],
                            'to' => (string)$day->time[1],
                        );

                    }

                }

        header("HTTP/1.0 200 OK");

                echo json_encode($data);

            }

        }

        exit;

    }

}
